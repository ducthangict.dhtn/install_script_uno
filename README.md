# [Unoserver](https://www.voducthang.com "Vo Duc Thang's Homepage") Install Script

## Requirement
1. Installed Libreoffice<br/>
2. Installed unoserver<br/>
Please follow if it do not exist on system.<br/>

```

pip install unoserver
pip install py3o.template
pip install py3o.formats

sudo apt-get --no-install-recommends install libreoffice
sudo apt-get install libreoffice-java-common default-jre

```

## Installation procedure

##### 1. Download the script:
```
git clone https://gitlab.com/ducthangict.dhtn/install_script_uno.git
```
##### 2. Modify the parameters as you wish.
No need
#### 3. Make the script executable
```
cd install_script_uno
sudo chmod +x unoserver_install.sh
```
##### 4. Execute the script:
```
./unoserver_install.sh
```

##### 5. Check services on system:
```
ps -ef | grep unoserver
```
If it's not exist please start it with commandline:
```
/etc/init.d/uno-server start
```

## Contributor
Name: Vo Duc Thang <br/>
Phone: 0989 464 344 <br/>
Website: https://voducthang.com <br/>
Skype: ictlucky.dhtn <br/>
